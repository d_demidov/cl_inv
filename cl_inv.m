function B = invmat(A)
    lib = 'libcl_inv';
    if not(libisloaded(lib))
        loadlibrary(lib, 'cl_inv.h');
    end

    [n, m] = size(A);

    assert(n == m, 'Non-square matrix in cl_inv!');

    B = zeros(n, n);

    B = calllib(lib, 'cl_inv', B, A', n);
    B = B';


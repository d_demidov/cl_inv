#ifndef CL_INVMAT_H
#define CL_INVMAT_H

#ifdef __cplusplus
extern "C" {
#endif

void cl_inv(double *b, const double *a, int n);

#ifdef __cplusplus
}
#endif

#endif

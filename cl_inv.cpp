#include <iostream>
#include <mutex>

#include <vexcl/devlist.hpp>
#include <vexcl/element_index.hpp>
#include <vexcl/vector.hpp>

#include <viennacl/matrix.hpp>
#include <viennacl/linalg/lu.hpp>

#include "cl_inv.h"

const vex::Context& ctx() {
    static vex::Context c(vex::Filter::Exclusive(vex::Filter::Env));

    static std::once_flag viennacl_initialized;

    std::call_once(viennacl_initialized, []() {
            std::cout << c << std::endl;

            viennacl::ocl::setup_context(0,
                    c.context(0)(),
                    c.device(0)(),
                    c.queue(0)());
            });
    
    return c;
}

void cl_inv(double *b, const double *a, int n) {
    using namespace vex;

    vex::vector<double> A(ctx(), n * n, a);
    vex::vector<double> I(ctx(), n * n);

    auto i = element_index();

    I = (i/n == i%n);

    viennacl::matrix<double> A_(A(0).raw(), n, n);
    viennacl::matrix<double> I_(I(0).raw(), n, n);

    viennacl::linalg::lu_factorize(A_);
    viennacl::linalg::lu_substitute(A_, I_);
 
    vex::copy(I.begin(), I.end(), b);
}
